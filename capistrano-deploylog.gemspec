# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano/deploylog/version'

Gem::Specification.new do |spec|
  spec.name          = "capistrano-deploylog"
  spec.version       = Capistrano::Deploylog::VERSION
  spec.authors       = ["Joren De Groof"]
  spec.email         = ["git@jorendegroof.be"]

  spec.summary       = %q{A Capistrano plugin for http://gitlab.com/joren/deploylog}
  spec.description   = %q{Log each deploy with Capistrano to your own deploylog application. Saves who deploys, when, what revision and what stage.}
  spec.homepage      = "https://gitlab.com/joren/capistrano-deploylog"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
