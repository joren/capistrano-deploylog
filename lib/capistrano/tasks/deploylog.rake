require "net/http"
require "uri"

namespace :deploylog do
  desc "Notify your deploylog app"
  task :notify_deploy_finished do
    on roles :deploylog do
      info "Starts notifying Deploylogger"
      Net::HTTP.post_form(
        deploylog_url,
        project_key: project_key,
        revision: revision,
        deployer: deployer,
        environment: environment,
        location: location
      )
      info "Finished notifying Deploylogger"
    end
  end

  def deploylog_url
    URI("#{fetch(:deploylog_endpoint)}/api/deploys")
  end

  def project_key
    fetch(:deploylog_project_id)
  end

  def revision
    fetch(:current_revision)
  end

  def deployer
    user = fetch(:deploylog_deployer)
    user ||= if (u = %x{git config user.name}.strip).to_s != ""
               u
             else
               "Someone"
             end
    user
  end

  def environment
    fetch(:deploylog_env, fetch(:rack_env, fetch(:rails_env, fetch(:stage))))
  end

  def location
    host.hostname || host
  end

  after "deploy:finished", "deploylog:notify_deploy_finished"
end
